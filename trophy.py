class trophy():
	def req_trophy(self,raw=False):
		res = self.req_login_param(raw=raw)
		return res if raw else res['trophyprogs']

	def exec_trophy(self,trophies,ymd=False, raw=False):
		if type(trophies) == dict:
			trophies = [trophies] 
		body = {
			"param":{
				"trophyprogs":self.trophyprogs(trophies,ymd)
				}
			}
		return self.api_request("/trophy/exec",body,raw=raw)

	def trophyprogs(self,trophies,ymd=False):
		if type(trophies)==dict:
			trophies=[trophies]
		timestamp=self.ymd_timestamp(ymd)
		return (
			[
				{
					"iname": trophy['iname'],
					"pts": [trophy['ival']] if 'ival' in trophy else [1],
					"ymd":timestamp,
					"rewarded_at":timestamp
				}
				for trophy in trophies
			]
			)