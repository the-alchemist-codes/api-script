class multiplayer():
	def req_multi_check(self,raw=False):
		return self.api_request('/btl/multi/check',raw=raw)	#device_id

	def req_multi_room(self,quest,raw=False):
		return self.api_request('btl/room',{"param": {"iname": quest}},raw=raw)	#empty arry

	def req_multi_room_make(self,quest,comment="",pwd="0",private=0,limit=0,unitlv=0,clear=0,raw=False):
		body = {
			"param": {
				"iname": quest,
				"comment": comment,
				"pwd": pwd,
				"private": int(private),
				"req_at": UNIX_timestamp(),
				"limit": int(limit),
				"unitlv": unitlv,
				"clear": int(clear)
				}
			}
		return self.api_request('/btl/room/make',body,raw=raw)	#roomid,app_id,token

	def req_multi_battle(self,quest,token,partyid=1,host=1,plid=1,seat=1,raw=False):
		body = {	
			"param": {
				"iname": quest,
				"partyid": partyid,
				"token": token,
				"host": str(host),
				"plid": str(plid),
				"seat": str(seat),
				"btlparam": {
					"help": {
						"fuid": ""
					}
					},
				"location": {
					"lat": 0,
					"lng": 0
					}
				}
			}
		return self.api_request('btl/multi/req',body,raw=raw)

	def end_multi_battle(self,btlid,token,beats,result="win",fuids=[],raw=False):
		body = {	
			"param": {
				"btlid": btlid,
				"btlendparam": {
					"time": 0,
					"result": result,
					"beats": beats,
					"steals": {
						"items": [0]*len(beats),
						"golds": [0]*len(beats)
						},
					"missions": [],
					"inputs": [],
					"token": token
					},
				"fuids": fuids
				}
			}
		return self.api_request('btl/multi/end',body,raw=raw)