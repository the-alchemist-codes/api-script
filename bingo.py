class bingo():
	def req_bingo(self,raw=False):
		res = self.req_login_param(raw=raw)
		return res if raw else res['bingoprogs']

	def exec_bingo(self,bingos,ymd=False,raw=False):
		if type(bingos) == dict:
			bingos = [bingos] 
		
		#multiple problems -> have to be solved in waves
		# 1. some bingos require others to be completed first - flg_quests
		# singular bingos have to be cleared for the board reward - parent_iname

		#sorting stuff into category - parent - child

		mbingos={}
		for bingo in bingos:
			iname=bingo['iname']
			category=bingo['category'] if 'category' in bingo else ''
			parent=bingo['parent_iname'] if 'parent_iname' in bingo else ''


			if category not in mbingos:
				mbingos[category]={}
			
			if parent:	#is child
				if parent not in mbingos[category]:
					mbingos[category][parent]={'childs':{}}
				mbingos[category][parent]['childs'][iname]=bingo
			else:	#is parent
				if iname not in mbingos[category]:
					mbingos[category][iname]={'childs':{}}
				mbingos[category][iname].update(bingo)	

		returns=[]
		cleared=[]
		while mbingos:	#loop stuff and destroy mbingos during it until is is destored
			sbingos=[]
			#generate send list
			del_category=[]
			for category,citems in mbingos.items():
				del_parent=[]
				for piname, parent in citems.items():
					exec=True
					#check if it can be run
					if 'flg_quests' in parent:
						for flag in parent['flg_quests']:
							if flag not in cleared:
								exec=False
								break
					if not exec:
						continue

					#check if board can be cleared or childs have to be cleared first
					if 'childs' in parent:
						#execute childs
						sbingos+=[child for ciname,child in parent['childs'].items()]
						del parent['childs']
					else:
						if parent:
							sbingos.append(parent)
						cleared.append(piname)
						del_parent.append(piname)

			#cleanup
				if del_parent:
					for piname in del_parent:
						del citems[piname]

					if not citems: #empty
						del_category.append(category)

			if del_category:
				for ciname in del_category:
					del mbingos[ciname]

			#request childs first
			body = {
				"param":{
					"bingoprogs":self.bingoprogs(sbingos,ymd)
					}
				}
			returns.append(self.api_request("/bingo/exec",body,raw=raw))

		return returns

	def bingoprogs(self,bingos,ymd=False):
		if type(bingos)==dict:
			bingos=[bingos]
		timestamp=self.ymd_timestamp(ymd)
		return (
			[
				{
					"iname": bingo['iname'],
					"parent": bingo['parent_iname'] if 'parent_iname' in bingo else '',
					"pts": [bingo['ival']] if 'ival' in bingo else [1],
					"ymd":timestamp,
					"rewarded_at":timestamp
				}
				for bingo in bingos
				if bingo
			]
			)