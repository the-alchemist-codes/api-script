class login():
	def req_chk_player(self,raw=False):
		body = {
			"access_token": "",
			"param": {
				"device_id": self.device_id,
				"secret_key": self.secret_key,
				"idfa": self.idfa,	# Google advertising ID
			}
		}
		res_body = self.api_request("/player/chkplayer", body,'POST',raw)
		try:
			ret = res_body['body']['result']
		except:
			print('error: failed to retrieve chklayer')
			ret=self.req_chk_player(raw)
		return ret

	def req_achieve_auth(self):
		return(self.api_connect('/achieve/auth',{},'GET'))

	def req_register(self):
		body={"access_token": "",
			"param": {
			"udid": "",
			"secret_key": self.secret_key,
			"idfv": self.idfv,
			"idfa": self.idfa
			}
		}
		res_body=self.api_connect('/gauth/register',body,'POST')
		try:
			ret = res_body['body']['device_id']
		except:
			print('error: failed to register')
			print(res_body)
			input('/')
			ret=self.req_register()
		return ret

	def req_playnew(self,raw=False,debug=False):
		body={
			"param": {
				"permanent_id": self.idfa,
				}
			}
		if debug:
			body['param']["debug"]=1
		return self.api_request('/playnew',body,'POST',raw)
			
	def req_home(self,raw=False):
		return self.api_request('/home',{"param": {"is_multi_push": 1}},raw=raw)
	
	def req_bundle(self,raw=False):
		return self.api_request('/bundle',raw=raw)

	def req_login(self,raw=False):
		if not self.cuid:
			login=self.api_request("/login",{"param":{"device":"HUAWEI HUAWEI MLA-L12","dlc":"apvr"}},raw=True)
			if login['body']:
				self.cuid=login['body']['player']['cuid']
				self.fuid=login['body']['player']['fuid']
			return login if raw else login['body']
		return self.api_request("/login",{"param":{"device":"HUAWEI HUAWEI MLA-L12","dlc":"apvr"}},raw=raw)

	def req_login_param(self,relogin=0,raw=False):
		return self.api_request('/login/param',{"param": {"relogin": int(relogin)}},raw=raw)

	def print_login(self,json_res=False):
		if not json_res:
			json_res=self.req_login()
		print("--------------------------------------------")
		print("Name:", json_res["player"]["name"])
		print("P. Lv:", json_res["player"]["lv"])
		print("User Code:", json_res["player"]["cuid"])
		print("Friend ID:", json_res["player"]["fuid"])
		print("Created at:", json_res["player"]["created_at"])
		print("Exp:", json_res["player"]["exp"])
		print("Stamina:", json_res["player"]["stamina"]["pt"], "/", json_res["player"]["stamina"]["max"])
		print("Zeni:", json_res["player"]["gold"])
		print("Gems:", json_res["player"]["coin"]["paid"], "Paid,", json_res["player"]["coin"]["com"], "Shared,",json_res["player"]["coin"]["free"], "Free")
		print("--------------------")
		return json_res