class shop():
	def req_shopslist(self,typ='',items=True,raw=False):	#types:	limited,event, no type
		res_body = self.api_request('/shop%s/shoplist'%('/%s'%typ if typ else ''),raw=raw)
		try:
			ret=res_body['body']['shops']
			if items:
				for shop in res_body["body"]["shops"]:
					shop["shopitems"]= self.req_shop(shop["gname"],typ,raw=True)
					if not raw:
						shop['shopitems']=shop['shopitems']["body"]["shopitems"]
		except:
			print('error: failed to request shops')
			print(res_body)
			raw=True
		return res_body if raw else ret

	def req_shop(self,shopName,typ='',raw=False):
		return self.api_request('/shop%s'%('/%s'%typ if typ else ''),{"param": {"shopName": shopName}},'POST',raw)

	def update_shop(self,shopName,typ='',raw=False):
		return self.api_request('/shop%s/update'%('/%s'%typ if typ else ''),{"param": {"iname": shopName}},raw=raw)

	def buy_shop(self,shopName,typ='',id=1,num=1,raw=False):
		body={
			"param": {
				"iname": shopName,
				"id": id,
				"buynum": num
				}
			}
		return self.api_request('/shop%s/buy'%('/%s'%typ if typ else ''),body,raw=raw)