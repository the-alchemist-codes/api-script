import TAC_API

class Friend(object):
    def __init__(self, friend_json={}, api='', fuid='', name=''):
        
#example
# {
# 	"friends": [
# 		{
# 			"fuid": "VLMPR1HH",
# 			"name": "Winsa",
# 			"type": "none",
# 			"lv": 181,
# 			"lastlogin": 1544026294,
# 			"unit": {
# 				"iid": 19759306,
# 				"iname": "UN_V2_BALT",
# 				"rare": 4,
# 				"plus": 25,
# 				"lv": 85,
# 				"exp": 5579088,
# 				"jobs": [
# 					{
# 						"iid": 27053079,
# 						"iname": "JB_THI",
# 						"rank": 11,
# 						"equips": [
# 							{
# 								"iid": 30803499,
# 								"iname": "IT_EQ_HOOK",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 30803500,
# 								"iname": "IT_EQ_ATK_SPEAR",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 30803501,
# 								"iname": "IT_EQ_DEF_HAGOROMO",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 30803502,
# 								"iname": "IT_EQ_DEX_COMPASS",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 30803503,
# 								"iname": "IT_EQ_SPD_BOOTS",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 30803504,
# 								"iname": "IT_EQ_STATUS_BIG",
# 								"exp": 0
# 							}
# 						],
# 						"abils": [
# 							{
# 								"iid": 27053080,
# 								"iname": "AB_THI_UPPER",
# 								"exp": 19
# 							},
# 							{
# 								"iid": 27053081,
# 								"iname": "AB_THI_LOWER",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 27053111,
# 								"iname": "AB_THI_MOVE1",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 27053172,
# 								"iname": "AB_THI_QUICK_ACTION",
# 								"exp": 19
# 							},
# 							{
# 								"iid": 27053217,
# 								"iname": "AB_THI_JUMP1",
# 								"exp": 0
# 							}
# 						],
# 						"artis": [
# 							{
# 								"iid": 27256075,
# 								"iname": "AF_ARMS_THI_03",
# 								"rare": 2,
# 								"exp": 0
# 							}
# 						],
# 						"cur_skin": "AF_SK_BALT_UNIQUE",
# 						"select": {
# 							"abils": [
# 								27053080,
# 								45341290,
# 								27053172,
# 								27053111,
# 								45341294
# 							],
# 							"artifacts": [
# 								27256075,
# 								0,
# 								0
# 							]
# 						}
# 					},
# 					{
# 						"iid": 38742419,
# 						"iname": "JB_BAA",
# 						"rank": 11,
# 						"equips": [
# 							{
# 								"iid": 43714194,
# 								"iname": "IT_EQ_DARKKABUTO",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 43714195,
# 								"iname": "IT_EQ_ATK_SPEAR",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 43714196,
# 								"iname": "IT_EQ_DEF_HAGOROMO",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 43714197,
# 								"iname": "IT_EQ_DEX_COMPASS",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 43714198,
# 								"iname": "IT_EQ_SPD_BOOTS",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 43714199,
# 								"iname": "IT_EQ_STATUS_BIG",
# 								"exp": 0
# 							}
# 						],
# 						"abils": [
# 							{
# 								"iid": 38742420,
# 								"iname": "AB_BAA_UPPER",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 38742421,
# 								"iname": "AB_BAA_LOWER",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 39597871,
# 								"iname": "AB_BAA_OVER_SOUL",
# 								"exp": 19
# 							},
# 							{
# 								"iid": 39597872,
# 								"iname": "AB_BAA_AVENGER",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 39597873,
# 								"iname": "AB_BAA_SOUL_SUCKER",
# 								"exp": 0
# 							}
# 						],
# 						"artis": [],
# 						"cur_skin": "AF_SK_BALT_UNIQUE",
# 						"select": {
# 							"abils": [
# 								38742420,
# 								19759309,
# 								21583942,
# 								21583926,
# 								39597871
# 							],
# 							"artifacts": [
# 								0,
# 								0,
# 								0
# 							]
# 						}
# 					},
# 					{
# 						"iid": 45341288,
# 						"iname": "JB_BALT_SOL",
# 						"rank": 11,
# 						"equips": [
# 							{
# 								"iid": 45341308,
# 								"iname": "IT_EQ_BALT_TWIN_SWORD",
# 								"exp": 90
# 							},
# 							{
# 								"iid": 45341309,
# 								"iname": "IT_EQ_BALT_BOOTS",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 45341310,
# 								"iname": "IT_EQ_BALT_BOTTOM",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 45341311,
# 								"iname": "IT_EQ_BALT_JACKET",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 45341312,
# 								"iname": "IT_EQ_BALT_BANDANA",
# 								"exp": 0
# 							},
# 							{
# 								"iid": 45341313,
# 								"iname": "IT_EQ_BALT_MEDALLION",
# 								"exp": 0
# 							}
# 						],
# 						"abils": [
# 							{
# 								"iid": 45341289,
# 								"iname": "AB_BALT_SOL_UPPER",
# 								"exp": 19
# 							},
# 							{
# 								"iid": 45341290,
# 								"iname": "AB_BALT_SOL_LOWER",
# 								"exp": 19
# 							},
# 							{
# 								"iid": 45341294,
# 								"iname": "AB_SOL_FEINT",
# 								"exp": 19
# 							},
# 							{
# 								"iid": 45341295,
# 								"iname": "AB_SOL_PERFECT_AVOID",
# 								"exp": 19
# 							}
# 						],
# 						"artis": [
# 							{
# 								"iid": 34460554,
# 								"iname": "AF_ARMS_SOL_04",
# 								"rare": 4,
# 								"exp": 82020
# 							},
# 							{
# 								"iid": 26943676,
# 								"iname": "AF_ACCS_CLOAK",
# 								"rare": 3,
# 								"exp": 43800
# 							},
# 							{
# 								"iid": 34038275,
# 								"iname": "AF_ACCS_GL_SIEG_HAT",
# 								"rare": 4,
# 								"exp": 82020
# 							}
# 						],
# 						"cur_skin": "AF_SK_BALT_UNIQUE",
# 						"select": {
# 							"abils": [
# 								45341289,
# 								45341290,
# 								45341295,
# 								45341294,
# 								39597871
# 							],
# 							"artifacts": [
# 								34460554,
# 								26943676,
# 								34038275
# 							]
# 						}
# 					}
# 				],
# 				"select": {
# 					"job": 45341288,
# 					"quests": [
# 						{
# 							"qtype": "coldef",
# 							"jiid": 45341288
# 						}
# 					]
# 				},
# 				"skin_unlocked": [
# 					"AF_SK_BALT_UNIQUE"
# 				],
# 				"quest_clear_unlocks": []
# 			}
# 		}
# 	]
# }