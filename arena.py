class arena():
	### Arena
	def req_arena(self,raw=False):
		return self.api_request("/btl/colo",raw=raw)

	def req_arena_ranking(self,raw=False):
		res_body = self.api_request("/btl/colo/ranking/world",{},'POST',True)
		try:
			ret = res_body['body']['coloenemies']
		except:
			print('error: failed to retrieve arena')
			print(res_body)
			raw=True
		return res_body if raw else ret

	def exec_arena(self,enemy_fuid,opp_rank,my_rank,result='win',beats=[1,1,1],trophies=False,bingos=False,raw=False):
		body={
			"param": {
				"fuid": enemy_fuid,
				"opp_rank": opp_rank,
				"my_rank": my_rank,
				"btlendparam": {
					"time": 0,
					"result": result,
					"beats": beats,
					"steals": {
						"items": [0, 0, 0],
						"golds": [0, 0, 0]
						},
					"missions": [],
					"inputs": []
					}
				}
			}
		if trophies:
			body["trophyprogs"]=self.trophyprogs(trophies)
		if bingos:
			body["bingoprogs"]=self.bingoprogs(bingos)
		
		return self.api_request('/btl/colo/exec',body,raw=raw)