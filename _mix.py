import uuid

from ._api import api
from .account import account
from .arena import arena
from .battle import battle
from .bingo import bingo
from .chat import chat
from .conceptcard import conceptcard
from .friend import friend
from .gacha import gacha
from .login import login
from .mail import mail
from .multiplayer import multiplayer
from .reroll import reroll
from .shop import shop
from .timestamp import timestamp
from .trophy import trophy

class tac_api2(api,account,arena,battle,bingo,chat,conceptcard,friend,login,mail,multiplayer,reroll,shop,timestamp,trophy):
	def __init__(self,api="app.alcww.gumi.sg",device_id='',secret_key='',idfa=str(uuid.uuid4()),idfv=str(uuid.uuid4())):
		object.__init__(self)
		self.api=api
		self.device_id=device_id
		self.secret_key=secret_key
		self.idfa=idfa
		self.idfv=idfv
		self.name=''
		self.ticket=0
		self.access_token=''
		self.cuid=''
		self.fuid=''