class mail():	
	def req_mail(self,page=1,period=1,read=0,raw=False):
		body = {
			"param": {
				"page": page,
				"isPeriod": period,
				"isRead": int(read)
				}
			}
		res = self.api_request('/mail',body,raw=raw)
		return res if raw else res['mails']

	def read_mail(self,mailIds,page=1,period=1,raw=False):
		if type(mailIds)!=list:
			mailIds=[int(mailIds)]
		body = {
			"param": {
				"mailids": mailIds,
				"page": page,
				"period": period
			}
		}
		return self.api_request('/mail/read',body,raw=raw)