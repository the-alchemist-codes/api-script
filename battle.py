class battle():	
	def req_quest_runs(self,raw=False):
		res = self.req_login_param(raw=raw)
		return res if raw else res['quests']

	def req_used_units(self,raw=False):
		return self.api_request('/btl/usedunit/multiple',{	"param": {"inames": ["quest", "arena", "tower_match"]}},raw=raw)
		#	returns array of {iname:,ranking:[unit_iname,job_iname,num],is_ready: just tells if can be used}

	### Single Player
	def req_quests(self,raw=False):
		res = self.api_request('/btl/com',{"param": {"event": 1}},raw=raw)
		return res if raw else res['quests']

	def req_battle(self,quest,partyid=0,fuid="",raw=False):
		body={
			"param": {
				"iname": quest,
				"partyid": partyid,
				"req_at": UNIX_timestamp(),
				"btlparam": {
					"help": {
						"fuid": fuid
						}
					},
				"location": {
					"lat": 0,
					"lng": 0
					}
				}
			}
		return self.api_request('/btl/com/req',body,raw=raw)

	def end_battle(self,btlid=0,beats=[],result='win',missions=[0,0,0],trophies=False,bingos=False,raw=False,battle=False):
		if battle:
			btlid=battle['btlid']
			beats=[1]*battle['btlinfo']['drops']
			
		body={
			"param": {
				"btlid": btlid,
				"btlendparam": {
					"time": 0,
					"result": result,
					"beats": beats,
					"steals": {
						"items": [0]*len(beats),
						"golds": [0]*len(beats)
						},
					"missions": missions,
					"inputs": []
					}
				}
			}
		if trophies:
			body["trophyprogs"]=self.trophyprogs(trophies)
		if bingos:
			body["bingoprogs"]=self.bingoprogs(bingos)

		return self.api_request('btl/com/end',body,raw=raw)


	