import http.client
import time
import uuid
import json

class api():
	def req_accesstoken(self,raw=False):
		self.ticket=0
		body = {
			"access_token": "",
			"param": {
				"device_id": self.device_id,
				"secret_key": self.secret_key,
				"idfa": self.idfa,	# Google advertising ID
				"idfv": self.idfv,
				"udid":""
			}
		}
		res = self.api_request("/gauth/accesstoken", body,'POST',True)
		if 'access_token' in res['body']:
			self.access_token=res['body']['access_token']
		else:
			print('Failed receiving access_token',res)
		return res if raw else res['body']
	####	CONNECTION ###########################################################
	def api_request(self,url,body={},request='POST',raw=False,retry=False):
		if url[0]!= '/':
			url='/%s'%url

		res_body=self.api_connect(url,body,request)
		
		try:
			ret = res_body['body']
		except:
			print('error: failed to retrieve %s'%url)
			print(res_body)
			if retry:
				ret=self.api_request(url,body,request,raw,retry=False)
			else:
				raw=True
		return res_body if raw else ret

	def api_connect(self,url, body={},request="POST",api=False, ignoreStat=False):
		if not api:
			api=self.api
		body['ticket']=self.ticket

		#create headers
		RID=str(uuid.uuid4()).replace('-','')
		headers={
			'X-GUMI-DEVICE-PLATFORM': 'android',
			'X-GUMI-DEVICE-OS': 'android',
			'X-Gumi-Game-Environment': 'sg_production',
			"X-GUMI-TRANSACTION": RID,
			'X-GUMI-REQUEST-ID': RID,
			'X-GUMI-CLIENT': 'gscc ver.0.1',
			'X-Gumi-User-Agent': json.dumps({
				"device_model":"HUAWEI HUAWEI MLA-L12",
				"device_vendor":"<unknown>",
				"os_info":"Android OS 4.4.2 / API-19 (HUAWEIMLA-L12/381180418)",
				"cpu_info":"ARMv7 VFPv3 NEON VMH","memory_size":"1.006GB"
				}),
			"User-Agent": "Dalvik/1.6.0 (Linux; U; Android 4.4.2; HUAWEI MLA-L12 Build/HUAWEIMLA-L12)",
			"X-Unity-Version": "5.3.6p1",
			"Content-Type": "application/json; charset=utf-8",
			"Host": api,
			"Connection": "Keep-Alive",
			"Accept-Encoding": "gzip",
			"Content-Length": len(json.dumps(body))
			}
		if url!="/gauth/accesstoken" and url!='/gauth/register':
			if self.access_token == "":
				self.access_token = self.req_accesstoken()['access_token']
			headers["Authorization"] = "gauth " + self.access_token

		print(api+url)
		try:
			con = http.client.HTTPSConnection(api)
			con.connect()
			con.request(request, url, json.dumps(body), headers)
			res_body = con.getresponse().read()
			#print(res_body)
			con.close()
		except http.client.RemoteDisconnected:
			return self.api_connect(url, body)
		try:
			json_res= json.loads(res_body)
			if not ignoreStat and json_res['stat'] == 5002:
				self.access_token=""
				json_res = self.api_connect(url, body)
			self.ticket+=1
			return(json_res)
		except Exception as e:
			print(e)
			print(url)
			print(res_body)
			if '504 Gateway Time-out' in str(res_body):
				print('Waiting 30s, then trying it again')
				time.sleep(30)
			else:
				input('Unknown Error')
			return self.api_connect(url, body)