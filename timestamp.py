import datetime
class timestamp():
	####	timestamps	######################################
	def ymd_timestamp(self,date=False,deltaH=8):
		if 'app.alcww.gumi.sg' in self.api:
			deltaH=8
		elif 'alchemist.gu3.jp' in self.api:
			deltaH=14
		return ymd_timestamp(date=date,deltaH=deltaH)

def ymd_timestamp(date=False,deltaH=0):
	if not date:
		return '{:%y%m%d}'.format(datetime.datetime.utcnow() - datetime.timedelta(hours=deltaH))
	else:
		return '{:%y%m%d}'.format(datetime.date(*date) - datetime.timedelta(hours=deltaH))

def UNIX_timestamp(date=False):
	if not date:
		return round((datetime.datetime.utcnow() - datetime.datetime(1970,1,1)).total_seconds())
	else:
		return round((datetime.date(*date) - datetime.datetime(1970,1,1)).total_seconds())