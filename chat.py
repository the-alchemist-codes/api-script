class chat():
	def req_chat(self,channel=1,limit=30,last_msg_id=0,raw=False):
		body={
			"param": {
				"start_id": 0,
				"channel": channel,
				"limit": limit,
				"exclude_id": last_msg_id,
				"is_multi_push": 1
				}
			}
		res = self.api_request('/chat/message',body,raw=raw)
		return  res if raw else res['messages']

	def req_chat_room(self,roomtoken=1,limit=30,last_msg_id=0,raw=False):
		body={
			"param": {
				"start_id": 0,
				"roomtoken": roomtoken,
				"limit": limit,
				"exclude_id": last_msg_id,
				"is_multi_push": 1
				}
			}
		res = self.api_request('/chat/room/message',body,raw=raw)
		return  res if raw else res['messages']