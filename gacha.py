class gacha():
	def req_gacha(self,raw=False):
		res=self.api_request("/gacha",{},'POST',raw)
		return res if raw else res['gachas']

	def exec_gacha(self,GachaID,raw=False):
		return self.api_request("/gacha/exec",{"param":{"gachaid":GachaID,"free":0}},'POST',raw)
