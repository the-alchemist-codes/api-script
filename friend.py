class friend():
	def req_friend_fuid(self,fuid,raw=False):
		return self.api_request('/friend/find',{'param':{'fuid':fuid}},'POST',raw)

	def req_friend_name(self,name,raw=False):
		return self.api_request('/friend/search',{'param':{'name':name}},'POST',raw)